(require 'ox-blackfriday)
(require 'ox-publish)


;;; Variables


;;; Define Back-End

(org-export-define-derived-backend 'agora 'blackfriday
  :translate-alist '((link . org-agora-link)
                     (headline . org-agora-headline)
                     (horizontal-rule . org-agora-horizontal-rule)
                     (inner-template . org-agora-inner-template)))


;;; Miscellaneous Helper Functions


;;; Transcode Functions

;;;; Link
; file and id links are treated as wikilinks, so we simply output [[title]].
; Everything else we output the path, plus the description if present.
; TODO: Doesn't properly output image links, and probably a bunch of other things.
(defun org-agora-link (link desc info)
	(let* ((type (org-element-property :type link))
	       (raw-path (org-element-property :path link))
	       (path (cond
		            ((member type '("http" "https" "ftp" "mailto"))
		             (concat type ":" raw-path))
		            (t raw-path))))
    (cond ((org-export-inline-image-p link org-html-inline-image-rules)
           (let ((path (cond ((not (string-equal type "file"))
			                        (concat type ":" raw-path))
			                       ((not (file-name-absolute-p raw-path)) raw-path)
			                       (t (expand-file-name raw-path))))
	               (caption (org-export-data
		                       (org-export-get-caption
		                        (org-export-get-parent-element link))
		                       info)))
	           (format "![[%s]]"
		                 (if (not (org-string-nw-p caption)) path
		                   (format "%s \"%s\"" path caption)))))
          ((member type '("file" "id")) (concat "[[" desc "]]"))
          ((member type '("roam")) (concat "[[" raw-path "]]"))
          (t (if (not desc) path
	             (format "[%s](%s)" desc path))))))

;;;; Headline
; This is a straight copy from org-md-headline, but adding 1 to the headline levels.
(defun org-agora-headline (headline contents info)
  "Transcode HEADLINE element into Markdown format.
CONTENTS is the headline contents.  INFO is a plist used as
a communication channel."
  (unless (org-element-property :footnote-section-p headline)
    (let* ((level (+ 1 (org-export-get-relative-level headline info)))
	   (title (org-export-data (org-element-property :title headline) info))
	   (todo (and (plist-get info :with-todo-keywords)
		      (let ((todo (org-element-property :todo-keyword
							headline)))
			(and todo (concat (org-export-data todo info) " ")))))
	   (tags (and (plist-get info :with-tags)
		      (let ((tag-list (org-export-get-tags headline info)))
			(and tag-list
			     (concat "     " (org-make-tag-string tag-list))))))
	   (priority
	    (and (plist-get info :with-priority)
		 (let ((char (org-element-property :priority headline)))
		   (and char (format "[#%c] " char)))))
	   ;; Headline text without tags.
	   (heading (concat todo priority title))
	   (style (plist-get info :md-headline-style)))
      (cond
       ;; Cannot create a headline.  Fall-back to a list.
       ((or (org-export-low-level-p headline info)
	    (not (memq style '(atx setext)))
	    (and (eq style 'atx) (> level 6))
	    (and (eq style 'setext) (> level 2)))
	(let ((bullet
	       (if (not (org-export-numbered-headline-p headline info)) "-"
		 (concat (number-to-string
			  (car (last (org-export-get-headline-number
				      headline info))))
			 "."))))
	  (concat bullet (make-string (- 4 (length bullet)) ?\s) heading tags "\n\n"
		  (and contents (replace-regexp-in-string "^" "    " contents)))))
       (t
	(let ((anchor
	       (and (org-md--headline-referred-p headline info)
		    (format "<a id=\"%s\"></a>"
			    (or (org-element-property :CUSTOM_ID headline)
				(org-export-get-reference headline info))))))
	  (concat (org-md--headline-title style level heading anchor tags)
		  contents)))))))

(defun org-agora-horizontal-rule (_horizontal-rule _contents _info)
  "Transcode HORIZONTAL-RULE element into Markdown format.
CONTENTS is the horizontal rule contents.  INFO is a plist used
as a communication channel."
  "***")

;;;; Inner template
(defun org-agora-inner-template (contents info)
  "Return complete document string after Markdown conversion.
CONTENTS is the transcoded contents string.  INFO is a plist used
as a communication channel."
  (concat
   ; Prepend the title to the rest of the contents
   (format "# %s\n\n" (car (plist-get info :title)))
   ;; Document contents.
   contents
   "\n"
   ;; Footnotes section.
   (org-md--footnote-section info)))


;;; Interactive functions

;;;###autoload
(defun org-agora-publish-to-agora (plist filename pub-dir)
  "Publish an Org file to Agora-compatible Markdown file.

PLIST is the property list for the given project.  FILENAME is
the filename of the Org file to be published.  PUB-DIR is the
publishing directory.

Return output file name."
  (org-publish-org-to 'agora filename ".md" plist pub-dir))


(provide 'ox-agora)


;;; ox-agora.el ends here
